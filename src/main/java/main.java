import java.util.*;

import static org.junit.Assert.assertTrue;

class main {

    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        Scanner file = new Scanner(System.in);

        while(file.hasNextLine()){
            String s = file.nextLine();
            if(s.equals("")){
                break;}
            list.add(s);
        }

    for(int i=0;i<list.size();i++){
        ArrayList<String> ans=PRINT(list);
        System.out.println(ans.get(i));
}
        file.close();

    }

    public static ArrayList<String> getIgnoredWords(ArrayList<String> Input){

        ArrayList<String> ans = new ArrayList<String>();
        int i = 0;
        while(!Input.get(i).equals("::")){
            ans.add(Input.get(i));
            i++;
        }
        return ans;
    }

    public static ArrayList<String> getContent(ArrayList<String> Input){

        ArrayList<String> ans = new ArrayList<String>();
        int i = 0;
        while(!Input.get(i).equals("::")){
            i++;
        }

        for(i = i+1;i<Input.size();i++){
            ans.add(Input.get(i));
        }
        return ans;
    }

    public static ArrayList<String> getKeys(ArrayList<String> Input){

        ArrayList<String> keys = new ArrayList<String>();
        ArrayList<String> content = getContent(Input);
        ArrayList<String> ignored = getIgnoredWords(Input);

        //put all words in keys
        for(int i = 0;i<content.size();i++){
            String line = content.get(i);
            for(String rep_keys: line.split(" ")){
                keys.add(rep_keys);
            }
        }

        //remove ignored words
        for(int i=0;i<ignored.size();i++){
            String s2=ignored.get(i);
            keys.removeIf(s -> s.equalsIgnoreCase(s2));
        }

        LinkedHashSet<String> hs = new LinkedHashSet<>(keys);
        ArrayList<String> listWithoutDuplicates = new ArrayList<>(hs);
        String str;

        //sort keys
        for(int i = 0;i<listWithoutDuplicates.size();i++){
            for(int j =i +1;j<listWithoutDuplicates.size();j++){

                String first = listWithoutDuplicates.get(i);

                if(first.compareToIgnoreCase(listWithoutDuplicates.get(j))>0){
                    str = first;
                    listWithoutDuplicates.set(i,listWithoutDuplicates.get(j));
                    listWithoutDuplicates.set(j,str);
                }
            }
        }
        return listWithoutDuplicates;
    }

    public static ArrayList<String> PRINT(ArrayList<String> Input){
        ArrayList<String> ans = new ArrayList<>();
        ArrayList<String> sortedKey = getKeys(Input);
        ArrayList<String> titles = getContent(Input);

        for(int i = 0;i<sortedKey.size();i++){

            String key = sortedKey.get(i);

            for(int j = 0;j<titles.size();j++){

                String Line = titles.get(j);

                if(Line.contains(key)){
                    int count = 0;
                    for (int pos = Line.indexOf(key); pos >= 0; pos = Line.indexOf(key, pos + 1))
                    {
                        count++;
                    }

                    while(count>=1){

                        int index = Line.indexOf(key);
                        Line = Line.toLowerCase();
                        Line = Line.replaceFirst(Line.substring(index,index+key.length()),key.toUpperCase());
                        ans.add(Line);
                        count--;

                        if(count>=1){

                            int index2 = Line.indexOf(key.toLowerCase());
                            Line = Line.toLowerCase();
                            Line = Line.replace(Line.substring(index2,index2+key.length()),key.toUpperCase());
                            Line = Line.replaceFirst(Line.substring(index,index+key.length()),key.toLowerCase());
                            ans.add(Line);
                            count--;
                        }
                    }
                }
            }
        }
        return ans;
    }

}
