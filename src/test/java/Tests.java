import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.*;

class Tests {

    public ArrayList<String> getTestList(){
        ArrayList<String> ans = new ArrayList<>();
        ans.add("is" );
        ans.add("the" );
        ans.add("of" );
        ans.add("and" );
        ans.add("as" );
        ans.add("a" );
        ans.add("but" );
        ans.add("::" );
        ans.add("Descent of Man" );
        ans.add("The Ascent of Man" );
        ans.add("The Old Man and The Sea" );
        ans.add("A Portrait of The Artist As a Young Man" );
        ans.add("A Man is a Man but Bubblesort IS A DOG" );
        return ans;
    }


    @Test
    void getIgnoreWordsTest() {

        ArrayList<String> ans = new ArrayList<>();
        ans = getTestList();

        ArrayList<String> expected= new ArrayList<>();
        expected.add("is");
        expected.add("the");
        expected.add("of");
        expected.add("and");
        expected.add("as");
        expected.add("a");
        expected.add("but");

        assertEquals(new main().getIgnoredWords(ans), expected);
    }

    @Test
    void getContentTest(){

        ArrayList<String> ans = new ArrayList<>();
        ans = getTestList();

        ArrayList<String> expected= new ArrayList<>();
        expected.add("Descent of Man" );
        expected.add("The Ascent of Man" );
        expected.add("The Old Man and The Sea" );
        expected.add("A Portrait of The Artist As a Young Man" );
        expected.add("A Man is a Man but Bubblesort IS A DOG" );

        assertEquals(new main().getContent(ans), expected);
    }

    @Test
    public void getKeysTest(){

        ArrayList<String> ans = new ArrayList<>();
        ans = getTestList();

        ArrayList<String> expected= new ArrayList<>();
        expected.add("Artist");
        expected.add("Ascent");
        expected.add("Bubblesort");
        expected.add("Descent");
        expected.add("DOG");
        expected.add("Man");
        expected.add("Old");
        expected.add("Portrait");
        expected.add("Sea");
        expected.add("Young");

        assertEquals(new main().getKeys(ans), expected);
    }

    @Test
    void testPRINT(){

        ArrayList<String> ans = new ArrayList<>();
        ans = getTestList();

        ArrayList<String> expected= new ArrayList<>();
        expected.add("a portrait of the ARTIST as a young man");
        expected.add("the ASCENT of man");
        expected.add("a man is a man but BUBBLESORT is a dog");
        expected.add("DESCENT of man");
        expected.add("a man is a man but bubblesort is a DOG");
        expected.add("descent of MAN");
        expected.add("the ascent of MAN");
        expected.add("the old MAN and the sea");
        expected.add("a portrait of the artist as a young MAN");
        expected.add("a MAN is a man but bubblesort is a dog");
        expected.add("a man is a MAN but bubblesort is a dog");
        expected.add("the OLD man and the sea");
        expected.add("a PORTRAIT of the artist as a young man");
        expected.add("the old man and the SEA");
        expected.add("a portrait of the artist as a YOUNG man");
        assertEquals(new main().PRINT(ans), expected);
    }





}